<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_anggota extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
 	public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('masuk') != TRUE){
            $url=base_url();
            redirect($url.'login');
        }
        $this->load->model('M_anggota');
        $this->load->model('M_wilayah');
        $this->load->model('M_berkas');
        $this->load->model('M_kegiatan');
  }
	public function index()
	{
		if($this->session->userdata('akses') == 'Adminstrator'){

		$data["anggota"] = $this->M_anggota->getAll();

		$this->load->view('anggota.php', $data);
		}else{
			echo "string";
		}
	}
	public function uploadKtp($id)
	{


		$filename = $_FILES['ktp']['name'];
		$rm = str_replace(' ', '', $filename);
		$file_element_name = 'KTP'.uniqid().$rm;


	// var_dump($_FILES);
	// var_dump($file_element_name);
      $config['upload_path'] = './asset/img/berkas/';
      $config['allowed_types'] = 'jpg|png|jpeg|gif';
      $config['max_width'] = '4480'; // pixel
      $config['max_height'] = '4480'; // pixel
      $config['file_name'] = $file_element_name; // pixel

      $this->load->library('upload', $config);

      $this->upload->initialize($config);

	    if (!empty($_FILES['ktp']['name'])) {
	        if ( $this->upload->do_upload('ktp') ) {
	            $foto = $this->upload->data();
	            $ktp = "KTP";
	            $ket = "Berkas Upload";
	            $uploaded = $this->M_anggota->upload_berkas($id,$ktp,$file_element_name, $ket);
	                $status = "success";
					$this->session->set_flashdata('success', 'Sukses upload file');
					redirect(base_url('verif/'.$id));

	        }else {
					$this->session->set_flashdata('error', 'Gagal upload file');
					// After that you need to used redirect function instead of load view such as 
					redirect(base_url('verif/'.$id));
	        }
	    }else {
					$this->session->set_flashdata('error', 'Gagal upload file');
					// After that you need to used redirect function instead of load view such as 
					redirect(base_url('verif/'.$id));
	    }
	}
	public function uploadBukti($id)
	{


		$filename = $_FILES['bukti']['name'];
		$rm = str_replace(' ', '', $filename);
		$file_element_name = 'BT'.uniqid().$rm;


	// var_dump($_FILES);
	// var_dump($file_element_name);
      $config['upload_path'] = './asset/img/berkas/';
      $config['allowed_types'] = 'jpg|png|jpeg|gif';
      $config['max_width'] = '4480'; // pixel
      $config['max_height'] = '4480'; // pixel
      $config['file_name'] = $file_element_name; // pixel

      $this->load->library('upload', $config);

      $this->upload->initialize($config);

	    if (!empty($_FILES['bukti']['name'])) {
	        if ( $this->upload->do_upload('bukti') ) {
	            $foto = $this->upload->data();
	            $ktp = "Bukti Transfer";
	            $ket = "Berkas Upload";
	            $uploaded = $this->M_anggota->upload_berkas($id,$ktp,$file_element_name, $ket);
	                $status = "success";
					$this->session->set_flashdata('success', 'Sukses upload file');
					redirect(base_url('verif/'.$id));

	        }else {
					$this->session->set_flashdata('error', 'Gagal upload file');
					// After that you need to used redirect function instead of load view such as 
					redirect(base_url('verif/'.$id));
	        }
	    }else {
					$this->session->set_flashdata('error', 'Gagal upload file');
					// After that you need to used redirect function instead of load view such as 
					redirect(base_url('verif/'.$id));
	    }
	}
	public function uploadKK($id)
	{

		$filename = $_FILES['kk']['name'];
		$rm = str_replace(' ', '', $filename);
		$file_element_name = 'KK'.uniqid().$rm;


	// var_dump($_FILES);
	// var_dump($file_element_name);
      $config['upload_path'] = './asset/img/berkas/';
      $config['allowed_types'] = 'jpg|png|jpeg|gif';
      $config['max_width'] = '4480'; // pixel
      $config['max_height'] = '4480'; // pixel
      $config['file_name'] = $file_element_name; // pixel

      $this->load->library('upload', $config);

      $this->upload->initialize($config);

	    if (!empty($_FILES['kk']['name'])) {
	        if ( $this->upload->do_upload('kk') ) {
	            $foto = $this->upload->data();
	            $ktp = "KK";
	            $ket = "Berkas Upload";
	            $uploaded = $this->M_anggota->upload_berkas($id,$ktp,$file_element_name, $ket);
	                $status = "success";
					$this->session->set_flashdata('success', 'Sukses upload file');
					redirect(base_url('verif/'.$id));

	        }else {
					$this->session->set_flashdata('error', 'Gagal upload file');
					// After that you need to used redirect function instead of load view such as 
					redirect(base_url('verif/'.$id));
	        }
	    }else {
					$this->session->set_flashdata('error', 'Gagal upload file');
					// After that you need to used redirect function instead of load view such as 
					redirect(base_url('verif/'.$id));
	    }
	}
	public function uploadFoto($id)
	{

		$filename = $_FILES['foto']['name'];
		$rm = str_replace(' ', '', $filename);
		$file_element_name = 'FT'.uniqid().$rm;

	// var_dump($_FILES);
	// var_dump($file_element_name);
      $config['upload_path'] = './asset/img/berkas/';
      $config['allowed_types'] = 'jpg|png|jpeg|gif';
      $config['max_width'] = '4480'; // pixel
      $config['max_height'] = '4480'; // pixel
      $config['file_name'] = $file_element_name; // pixel

      $this->load->library('upload', $config);

      $this->upload->initialize($config);

	    if (!empty($_FILES['foto']['name'])) {
	        if ( $this->upload->do_upload('foto') ) {
	            $foto = $this->upload->data();
	            $ktp = "foto";
	            $ket = "Berkas Upload";
	            $uploaded = $this->M_anggota->upload_berkas($id,$ktp,$file_element_name	, $ket);
	                $status = "success";
					$this->session->set_flashdata('success', 'Sukses upload file');
					$this->db->query("UPDATE tb_anggota SET foto = '$file_element_name' WHERE no_kta = '$id' ");
					redirect(base_url('verif/'.$id));

	        }else {
					$this->session->set_flashdata('error', 'Gagal upload file');
					// After that you need to used redirect function instead of load view such as 
					redirect(base_url('verif/'.$id));
	        }
	    }else {
					$this->session->set_flashdata('error', 'Gagal upload file');
					// After that you need to used redirect function instead of load view such as 
					redirect(base_url('verif/'.$id));
	    }
	}
	public function dashboard()
	{
			$id = $_SESSION['ses_id'];
			$data['chart'] = $this->M_anggota->chart();
			$data['lk'] = $this->M_anggota->g_lk();
			$data['pr'] = $this->M_anggota->g_pr();
			$data['k'] = $this->M_anggota->count_kegiatan();

			$data['profile'] = $this->M_anggota->getById($id);
			$this->load->view('dashboard', $data);
	}
	public function verif($id)
	{
		$data['ktp'] = $this->M_anggota->getKTP($id);
		$data['kk'] = $this->M_anggota->getKK($id);
		$data['foto'] = $this->M_anggota->getFoto($id);
		$data['bukti'] = $this->M_anggota->getBukti($id);
		$data['id'] = $id;
		$this->load->view('layout/header.php');
		$this->load->view('layout/nav.php');
		$this->load->view('form/verif_berkas.php', $data);
		$this->load->view('layout/footer.php');
	}
	public function detail($id=null)
	{
			$data["det"] = $this->M_anggota->detail($id);
			$data["riwayat"] = $this->M_berkas->getbyid($id);
			$data["berkas"] = $this->M_berkas->getberkas($id);

			$this->load->view('layout/header.php');
			$this->load->view('layout/nav.php');
			$this->load->view('det_anggota.php', $data);
			$this->load->view('layout/footer.php');
	}
	public function add()
	{
		if($this->session->userdata('akses') == 'Adminstrator'){
			$data['wil'] = $this->M_wilayah->getAll();
			$this->load->view('layout/header.php');
			$this->load->view('layout/nav.php');
			$this->load->view('form/adda.php', $data);
			$this->load->view('layout/footer.php');
		}else{
			
			redirect(base_url('404'));
		}
	}
	public function save()
	{
		$data = array(
			'id' 			=> uniqid(),
			'no_kta' 		=> date('ymdHis'),
			'foto' 			=> 'default.png',
			'nama_anggota' 	=> $this->input->post('nama'),
			'tempat_lahir' 	=> $this->input->post('tempat_lahir'),
			'tgl_lahir' 	=> $this->input->post('tgl_lahir'),
			'wilayah' 		=> $this->input->post('wilayah'),
			'telp' 			=> $this->input->post('telp'),
			'email' 		=> $this->input->post('email'),
			'nik' 			=> $this->input->post('nik'),
			'alamat' 		=> $this->input->post('alamat'),
			'desa' 			=> $this->input->post('kelurahan'),
			'kec' 			=> $this->input->post('kec'),
			'kab' 			=> $this->input->post('kab'),
			'prov' 			=> $this->input->post('prov'),
			'isdelete' 		=> 1,
			'jk' 			=> $this->input->post('jk'),
			'pos' 			=> $this->input->post('pos'),
			'status' 		=> 0,
			'tgl_gabung' 	=> date('Y-m-d')
			);
		
		$query = $this->M_anggota->save($data,'tb_anggota');
		
		$id = $data['no_kta'];

		if(isset($_POST['nama_or'])){
			for ($i=0; $i < count($_POST['nama_or']); $i++) { 
				$na = $_POST['nama_or'][$i];
				$ja = $_POST['jabatan'][$i];
				$pr = $_POST['periode'][$i];

				$this->db->query("INSERT INTO riwayat_organisasi VALUES ('$id','$na','$ja','$pr')");
			}
		}
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
			
			redirect(base_url('anggota'));
	}
	public function delete_ktp($id=null)
    {
        if (!isset($id)){ 
        	show_404();
        }

        $file = $this->db->get_where('det_anggota', ['id_anggota' => $id, 'nama_berkas'=> 'KTP'])->row();

        $query = $this->db->delete('det_anggota', ['id_anggota'=>$id, 'nama_berkas'=> 'KTP']);

        if($query){

            unlink("asset/img/berkas/".$file->berkas);

        }

        redirect(base_url('verif/'.$id));
    }
	public function delete_foto($id=null)
    {
        if (!isset($id)){ 
        	show_404();
        }

        $file = $this->db->get_where('det_anggota', ['id_anggota' => $id, 'nama_berkas'=> 'foto'])->row();

        $query = $this->db->delete('det_anggota', ['id_anggota'=>$id, 'nama_berkas'=> 'foto']);

        if($query){

            unlink("asset/img/berkas/".$file->berkas);

        }

        redirect(base_url('verif/'.$id));
    }
	public function delete_bukti($id=null)
    {
        if (!isset($id)){ 
        	show_404();
        }

        $file = $this->db->get_where('det_anggota', ['id_anggota' => $id, 'nama_berkas'=> 'Bukti Transfer'])->row();

        $query = $this->db->delete('det_anggota', ['id_anggota'=>$id, 'nama_berkas'=> 'Bukti Transfer']);

        if($query){

            unlink("asset/img/berkas/".$file->berkas);

        }

        redirect(base_url('verif/'.$id));
    }
	public function delete_kk($id=null)
    {
        if (!isset($id)){ 
        	show_404();
        }

        $file = $this->db->get_where('det_anggota', ['id_anggota' => $id, 'nama_berkas'=> 'KK'])->row();

        $query = $this->db->delete('det_anggota', ['id_anggota'=>$id, 'nama_berkas'=> 'KK']);

        if($query){

            unlink("asset/img/berkas/".$file->berkas);

        }

        redirect(base_url('verif/'.$id));
    }
	public function delete($id=null)
    {
        if (!isset($id)){ 
        	show_404();
        }
        echo $id;
        if ($this->M_anggota->delete($id)) {
        	$this->session->set_flashdata('success', 'Data berhasil dihapus');
            redirect(base_url('anggota'));
        }else{
        	$this->session->set_flashdata('error', 'Data gagal dihapus');
			redirect("anggota");
        }
    }
    public function form_edit($id=null)
    {
			$data['wil'] = $this->M_wilayah->getAll();
			$data['value'] = $this->M_anggota->getById($id);

			$this->load->view('layout/header.php');
			$this->load->view('layout/nav.php');
			$this->load->view('form/form_edit_anggota.php', $data);
			$this->load->view('layout/footer.php');
    }
    public function update()
    {
		$kta		= $_POST['kta'];
		$nama		= $_POST['nama'];
		$tempat		= $_POST['tempat_lahir'];
		$tgl		= $_POST['tgl_lahir'];
		$telp		= $_POST['telp'];
		$email		= $_POST['email'];
		$jk		 	= $_POST['jk'];
		$wilayah	= $_POST['wilayah'];
		$alamat		= $_POST['alamat'];
		$kelurahan	= $_POST['kelurahan'];
		$kec		= $_POST['kec'];
		$kab		= $_POST['kab'];
		$prov		= $_POST['prov'];
		$pos		= $_POST['pos'];
		$stat 		= $_POST['stat'];
		$nik 		= $_POST['nik'];
		
		$sql = $this->db->query("UPDATE tb_anggota SET 
				nama_anggota  	='$nama',   
				tempat_lahir  	='$tempat',
				tgl_lahir  		='$tgl',
				telp  			='$telp',
				email  			='$email',
				jk  			='$jk',
				wilayah  		='$wilayah',
				alamat  		='$alamat',
				desa  			='$kelurahan',
				kec  			='$kec',
				kab  			='$kab',
				prov  			='$prov',
				pos  			='$pos',
				status  		='$stat',
				nik  		='$nik'

				WHERE no_kta = '$kta'
			");

		if($_SESSION['akses'] == 'Adminstrator'){
			redirect(base_url('/anggota'));
		}else{
			redirect(base_url('anggota/detail/'.$_SESSION['ses_id'].''));
		}

    }
   	public function report_member()
   	{	
		if($this->session->userdata('akses') == 'Adminstrator'){
	   		if(isset($_GET['wil'])){
	   		$id = $_GET['wil'];
			$data['anggota'] = $this->M_anggota->getByWil($id);
			$data['nw'] = $this->M_wilayah->getById($id);
	   		}
			$data['wil'] = $this->M_wilayah->getAll();

	   		$this->load->view('report/anggota', $data);
		}else{

			redirect(base_url('404'));

		}
   		
   	}
   	public function cetak_member($id=null)
   	{
		if($this->session->userdata('akses') == 'Adminstrator'){
			$data['nw'] = $this->M_wilayah->getById($id);
			$data['anggota'] = $this->M_anggota->getByWil($id);
	   		$this->load->view('report/c_anggota', $data);
		}else{

			redirect(base_url('404'));

		}

   	}
   	public function report_activity()
   	{
		if($this->session->userdata('akses') == 'Adminstrator'){
	   		if(isset($_GET['action'])){
	   			$awal  = $_GET['awal'];
	   			$akhir  = $_GET['akhir'];
				$data['rep'] = $this->M_kegiatan->getBy($awal, $akhir);
	   		}else{
				$data['rep'] = $this->M_kegiatan->getAll();
	   		}
	   		$this->load->view('report/aktifitas', $data);
		}else{

			redirect(base_url('404'));

		}


   	}
   	public function cetak_kegiatan()
   	{
		if($this->session->userdata('akses') == 'Adminstrator'){
	   		if(isset($_GET['awal'])){
	   			$awal  = $_GET['awal'];
	   			$akhir  = $_GET['akhir'];
				$data['rep'] = $this->M_kegiatan->getBy($awal, $akhir);
	   		}else{
				$data['rep'] = $this->M_kegiatan->getAll();
	   		}
	   		$this->load->view('report/c_kegiatan', $data);
		}else{

			redirect(base_url('404'));

		}

   	}


}