<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_kegiatan extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    function __construct(){
        parent::__construct();
        $this->load->model("M_kegiatan");
        $this->load->helper('form');
    }
    public function index()
    {
        $data['kegiatan'] = $this->M_kegiatan->getAll();
        $this->load->view('kegiatan', $data);
    }
    public function add()
    {
        $this->load->view('form/add-kegiatan');
    }
    public function save()
    {

     $filename = $_FILES['documen']['name'];
     $rm = str_replace(' ', '', $filename);
     $file_element_name = 'file'.uniqid().$rm;

      $config['upload_path']    = './asset/img/berkas/doc_kegiatan/';
      $config['allowed_types']  = 'gif|jpg|png|doc|docx|pdf';
      $config['max_width']      = '4480'; // pixel
      $config['max_height']     = '4480'; // pixel
      $config['file_name']      = $file_element_name; // pixel

      $this->load->library('upload', $config);

      $this->upload->initialize($config);

        if (!empty($_FILES['documen']['name'])) {
            if ( $this->upload->do_upload('documen') ) {
                $documen = $this->upload->data();
                // $ktp = "foto";
                // $ket = "Berkas Upload";
                $this->M_kegiatan->save($file_element_name);
                    $status = "success";
                    $this->session->set_flashdata('success', 'Sukses simpan data');
                    redirect(base_url('kegiatan'));

            }else {
                    $this->session->set_flashdata('error', 'Gagal simpan file');
                    // After that you need to used redirect function instead of load view such as 
                    redirect(base_url('kegiatan'));
            }
        }else {
                    $this->session->set_flashdata('error', 'Gagal upload file');
                    // After that you need to used redirect function instead of load view such as 
                    redirect(base_url('kegiatan'));
        }

    }
    // public function delete($id=null)
    // {
    //     if($this->M_wilayah->destroy($id)){
    //         $this->session->set_flashdata('success', 'Data berhasil dihapus');
    //     }else{
    //         $this->session->set_flashdata('error', 'Data gagal dihapus');
    //     }
    //     redirect(base_url('wilayah'));        
    // }
    public function edit($id='')
    {
        $data['list'] = $this->M_kegiatan->getById($id);
        $this->load->view('form/edit-kegiatan', $data);
    }
    public function update()
    {

     $path = './asset/img/berkas/doc_kegiatan/';
     $filename = $_FILES['documen']['name'];
     $rm = str_replace(' ', '', $filename);
     $file_element_name = 'file'.uniqid().$rm;

      $config['upload_path']    = './asset/img/berkas/doc_kegiatan/';
      $config['allowed_types']  = 'gif|jpg|png|doc|docx|pdf';
      $config['max_width']      = '4480'; // pixel
      $config['max_height']     = '4480'; // pixel
      $config['file_name']      = $file_element_name; // pixel

      $this->load->library('upload', $config);

      $this->upload->initialize($config);

        if (!empty($_FILES['documen']['name'])) {
            if ( $this->upload->do_upload('documen') ) {
                $documen = $this->upload->data();
                @unlink($path.$this->input->post('filelama'));
                $status = "success";
                $f_name = $file_element_name;
                    $this->M_kegiatan->update($f_name);
                    $this->session->set_flashdata('success', 'Sukses perbarui data');
                    redirect(base_url('kegiatan'));

            }else {
                
                    $this->session->set_flashdata('error', 'Gagal perbarui file');
                    redirect(base_url('kegiatan'));
            }
        }else {
                    $f_name = $this->input->post('filelama');
                    $this->M_kegiatan->update($f_name);
                    $this->session->set_flashdata('success', 'Sukses perbarui data');
                    redirect(base_url('kegiatan'));
                    // After that you need to used redirect function instead of load view such as 
                    redirect(base_url('kegiatan'));
        }    
    }
    public function delete($id='')
    {
        if($this->M_kegiatan->destroy($id)){
            $this->session->set_flashdata('success', 'Data berhasil dihapus');
        }else{
            $this->session->set_flashdata('error', 'Data gagal dihapus');
        }
        redirect(base_url('kegiatan'));        
    }
    // public function update_wil($id='')
    // {
    //     if($this->M_wilayah->update($id)){
    //         $this->session->set_flashdata('success', 'Data berhasil diperbarui');
    //     }else{
    //         $this->session->set_flashdata('error', 'Data gagal diperbarui');
    //     }
    //     redirect(base_url('wilayah'));        
    // }

}
