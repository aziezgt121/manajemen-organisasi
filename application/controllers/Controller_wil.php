<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_wil extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    function __construct(){
        parent::__construct();
        $this->load->model("M_wilayah");
    }
    public function index()
    {
        $data['wil'] = $this->M_wilayah->getAll();
        $this->load->view('wilayah', $data);
    }
    public function add()
    {
        $this->load->view('form/add-wil');
    }
    public function save_wil()
    {
        if($query = $this->M_wilayah->save()){
            $this->session->set_flashdata('success', 'Data berhasil disimpan');
            redirect(base_url('wilayah'));        
        }else{
            $this->session->set_flashdata('error', 'Data gagal disimpan');
            redirect(base_url('wilayah'));        
        }
    }
    public function delete($id=null)
    {
        if($this->M_wilayah->destroy($id)){
            $this->session->set_flashdata('success', 'Data berhasil dihapus');
        }else{
            $this->session->set_flashdata('error', 'Data gagal dihapus');
        }
        redirect(base_url('wilayah'));        
    }
    public function edit($id='')
    {
        $data['wil'] = $this->M_wilayah->getById($id);
        $this->load->view('form/edit_wil', $data);
    }
    public function update_wil($id='')
    {
        if($this->M_wilayah->update($id)){
            $this->session->set_flashdata('success', 'Data berhasil diperbarui');
        }else{
            $this->session->set_flashdata('error', 'Data gagal diperbarui');
        }
        redirect(base_url('wilayah'));        
    }

}
