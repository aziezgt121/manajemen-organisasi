<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    function __construct(){
        parent::__construct();
        $this->load->model('M_login');
        $this->load->library("session");
    }
	public function index()
	{
		$this->load->view('login');
	}
    public function register()
    {
        $this->load->view('register');
    }
 
    public function login(){
        $username=htmlspecialchars($this->input->post('username',TRUE),ENT_QUOTES);
        $pass=htmlspecialchars($this->input->post('password',TRUE),ENT_QUOTES);
        $password = md5($pass);
 
        $cek_pengurus=$this->M_login->auth_pengurus($username,$password);
 
        if($cek_pengurus->num_rows() > 0){
                $data = $cek_pengurus->row_array();
                $this->session->set_userdata('masuk',TRUE);
                 if($data['level']=='1'){ 
                    $this->session->set_userdata('akses','Adminstrator');
                    $this->session->set_userdata('ses_id',$data['anggota']);
                    $this->session->set_userdata('ses_nama',$data['nama']);
                    redirect('dashboard');
                 }else{ 
                    $this->session->set_userdata('akses','Calon Anggota');
                    $this->session->set_userdata('ses_id',$data['anggota']);
                    $this->session->set_userdata('ses_nama',$data['nama']);
                    redirect('dashboard');
                 }
 
        }else{ //jika login sebagai mahasiswa
                $url=base_url();
                // $this->session->set_flashdata('msg','Username Atau Password Salah');
                $this->session->set_flashdata('warning', 'Warning Message...');
                redirect($url).'login';
        }
 
    }
    public function register_action()
    {
        $this->M_login->register();
    }
    public function logout(){
        $this->session->sess_destroy();
        $url=base_url('/login');
        redirect($url);
    }
    public function e404()
    {
        $this->load->view('404');
    }


}
