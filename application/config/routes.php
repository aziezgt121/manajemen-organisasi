<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Controller_anggota/dashboard';
$route['logout'] = 'Auth/logout';
$route['login'] = 'Auth/index';
$route['register'] = 'Auth/register';
$route['dashboard'] = 'Controller_anggota/dashboard';

$route['wilayah'] = 'Controller_wil/index';
$route['form-edit/wilayah/(:num)'] = 'Controller_wil/edit/$1';
$route['wilayah/delete/(:num)'] = 'Controller_wil/delete/$1';
$route['add-wilayah'] = 'Controller_wil/add';


$route['verif/(:num)'] 	= 'Controller_anggota/verif/$1';
$route['verif/delete/(:num)'] 	= 'Controller_anggota/delete_ktp/$1';
$route['verif/deletekk/(:num)'] 	= 'Controller_anggota/delete_kk/$1';
$route['verif/deletefoto/(:num)'] 	= 'Controller_anggota/delete_foto/$1';
$route['verif/deletebukti/(:num)'] 	= 'Controller_anggota/delete_bukti/$1';

$route['anggota'] = 'Controller_anggota/index';
$route['add-anggota'] = 'Controller_anggota/add';
$route['anggota/detail/(:num)'] = 'Controller_anggota/detail/$1';
$route['form-edit/anggota/(:num)'] = 'Controller_anggota/form_edit/$1';
$route['anggota/delete/(:num)'] = 'Controller_anggota/delete/$1';

$route['kegiatan'] = 'Controller_kegiatan/index';
$route['add-kegiatan'] = 'Controller_kegiatan/add';
$route['form-edit/kegiatan/(:num)'] = 'Controller_kegiatan/edit/$1';
$route['kegiatan/delete/(:num)'] = 'Controller_kegiatan/delete/$1';

$route['report-member/(:num)'] = 'Controller_anggota/report_member/$1';
$route['cetak-member/(:num)'] = 'Controller_anggota/cetak_member/$1';
$route['report-member'] = 'Controller_anggota/report_member';
$route['report-activity'] = 'Controller_anggota/report_activity';
$route['cetak-kegiatan'] = 'Controller_anggota/cetak_kegiatan';

$route['404'] = 'Auth/e404';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;