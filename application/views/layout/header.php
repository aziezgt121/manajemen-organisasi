<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Sistem Informasi Pendaftaran Anggota PAO</title>

    <!-- Custom fonts for this template -->
    <link href="<?= base_url('asset') ?>/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?= base_url('asset') ?>/css/sb-admin-2.min.css" rel="stylesheet">

    <!-- Custom styles for this page -->
    <link href="<?= base_url('asset') ?>/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    
    <!-- Bootstrap CSS -->
    <style type="text/css">
    .card-header {
      /*background-image: repeating-linear-gradient(#00FA9A, #FFFF00 10%, #FF4500 20%);*/
      background-image: linear-gradient(to right, rgba(255,0,0,0), rgba(255,0,0,1));
    }
</style>

   

</head>




