                <div class="container-fluid">
                             <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3" style="background-color: #2F4F4F">
                            <h6 class="m-2 font-weight-bold text-light">Detail Data Anggota</h6>
                        </div>
                        <div class="card-body">
                        <div style="text-align: center;">
                            <img src="<?php echo base_url() ?>asset/img/cop.png" style="width: 90%; height: 150px;"> 
                        </div>
                        <?php foreach ($det as $d) { ?>
                        <form class="pl-4 pr-4 ml-5 mr-5" action="action/add-anggota.php" method="POST" enctype="multipart/form-data">
                        <hr>
                        <img src="<?php echo base_url() ?>asset/img/berkas/<?= $d->foto ?>" style="width: 140px;" class="img-thumbnail mb-3">
                        <div class="alert alert-danger" role="alert">
                          Data Diri Calon Anggota
                        </div>
                          <div class="form-row">
                            <div class="form-group col-md-6">
                              <label for="inputEmail4">Nama Lengkap</label>
                              <input class="form-control" name="nik" required="" value="<?= $d->nama_anggota ?>" readonly>
                            </div>
                            <div class="form-group col-md-6">
                              <label for="inputPassword4">No NIK / NIP</label>
                              <input class="form-control" name="nik" required="" value="<?= $d->nik ?>" readonly>
                            </div>
                          </div>    
                          <div class="form-row">
                            <div class="form-group col-md-6">
                              <label for="inputEmail4">Tempat Lahir</label>
                              <input class="form-control" name="nik" required="" value="<?= $d->tempat_lahir ?>" readonly>

                            </div>
                            <div class="form-group col-md-6">
                              <label for="inputPassword4">Tanggal Lahir</label>
                              <input class="form-control" name="nik" required="" value="<?= $d->tgl_lahir ?>" readonly>

                            </div>
                          </div>
                          <div class="form-row">
                            <div class="form-group col-md-6">
                              <label for="inputEmail4">Telp / HP</label>
                              <input class="form-control" name="nik" required="" value="<?= $d->telp ?>" readonly>

                            </div>
                            <div class="form-group col-md-6">
                              <label for="inputPassword4">Email</label>
                              <input class="form-control" name="nik" required="" value="<?= $d->email ?>" readonly>

                            </div>
                          </div>    
                          <div class="form-row">
                            <div class="form-group col-md-6">
                              <label for="inputEmail4">Jenis Kelamin</label>
                              <input class="form-control" name="nik" required="" value="<?= $d->jk ?>" readonly>
                            </div>
                            <div class="form-group col-md-6">
                              <label for="inputPassword4">Permohono Anggota Untuk Wilayah</label>
                              <input class="form-control" name="nik" required="" value="<?= $d->wil ?>" readonly>
                            </div>
                          </div>                          
                          <div class="form-group">
                            <label for="inputAddress">Alamat</label>
                            <textarea class="form-control" rows="3" readonly=""><?= $d->alamat ?></textarea>
                          </div>
                          <div class="form-row">
                            <div class="form-group col">
                              <label for="inputCity">Kelurahan / Desa</label>
                              <input class="form-control" name="nik" required="" value="<?= $d->desa ?>" readonly>

                            </div>
                            <div class="form-group col">
                              <label for="inputCity">Kecamatan</label>
                              <input class="form-control" name="nik" required="" value="<?= $d->kec ?>" readonly>

                            </div>
                            <div class="form-group col">
                              <label for="inputCity">Kabupaten</label>
                              <input class="form-control" name="nik" required="" value="<?= $d->kab ?>" readonly>
                            </div>
                            <div class="form-group col">
                              <label for="inputCity">Provinsi</label>
                              <input class="form-control" name="nik" required="" value="<?= $d->prov ?>" readonly>

                            </div>
                            <div class="form-group col-md-2">
                              <label for="inputZip">Kode Pos</label>
                              <input class="form-control" name="nik" required="" value="<?= $d->pos ?>" readonly>
                            </div>
                          </div>
                        <div class="alert alert-danger" role="alert">
                          Riwayat Organisasi
                        </div>
                            <table class="table table-hover table-bordered" id="dinamis">
                                <thead>
                                    <tr bgcolor="#2F4F4F">
                                        <th width="570">Nama Organisasi</th>
                                        <th>Jabatan</th>
                                        <th>Priode</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($riwayat as $key => $d) { ?>
                                    <tr>
                                        <td><?= $d->organisais ?></td>
                                        <td><?= $d->jabatan ?></td>
                                        <td><?= $d->priode ?></td>
                                      </tr>
                                  <?php } ?>
                                </tbody>
                            </table>
                        <div class="alert alert-danger" role="alert">
                          Dokumen Pendaftaran
                        </div>
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Nama Berkas</th>
                                        <th>Berkas</th>
                                        <th>Keterangan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($berkas as $key => $val){ ?>
                                    <tr>
                                        <td><?= $val->nama_berkas ?></td>
                                        <td><a href="<?= base_url() ?>asset/img/berkas/<?= $val->berkas ?>" target="_blank"><i class="fa fa-file"></i> <?= $val->berkas ?></a></td>
                                        <td><?= $val->ket ?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table><hr>

                            <?php if($_SESSION['akses'] == 'Adminstrator'){ ?>    
                              <a href="<?= base_url('anggota') ?>" class="btn btn-danger btn-sm mb-5">Kembali</a>
                            <?php }else { ?>
                              <a href="<?= base_url('anggota/detail/'.$_SESSION
                              ['ses_id'].'') ?>" class="btn btn-danger btn-sm mb-5">Kembali</a>
                            <?php } ?>

                            <?php if($this->session->userdata('akses') != 'Adminstrator'){ ?>
                            <a href="<?php echo base_url('form-edit/anggota/'.$_SESSION['ses_id'].'') ?>" class="btn btn-primary btn-sm mb-5"> Perbarui</a>
                            <?php } ?>
                        </div>
                      </div>

                      <?php } ?>
