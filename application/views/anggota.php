                <?php  
                    $this->load->view('layout/header.php');
                    $this->load->view('layout/nav.php');
                ?>
                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <!-- DataTales Example -->
                    <?php if ($this->session->flashdata('category_success')) { ?>
                        <div class="alert alert-success"> <?= $this->session->flashdata('category_success') ?> </div>
                    <?php } ?>

                    <?php if ($this->session->flashdata('category_error')) { ?>
                        <div class="alert alert-danger"> <?= $this->session->flashdata('category_error') ?> </div>
                    <?php } ?>
                    <div class="card shadow mb-4">
                        <div class="card-header py-3" style="background-color: #2F4F4F">
                            <h6 class="m-2 font-weight-bold text-light">Data Anggota</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                            <a href="<?php echo base_url() ?>add-anggota" class="btn btn-danger btn-sm mb-3">Tambah Data</a>
                        <?php if($this->session->flashdata('success')){ ?>
                          <div class="alert alert-success"><?= $this->session->flashdata('success'); ?></div>
                        <?php } ?>

                        <?php if($this->session->flashdata('error')){ ?>
                          <div class="alert alert-danger"><?= $this->session->flashdata('error'); ?></div>
                        <?php } ?>
                                <table class="table table-bordered table-striped" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th width="1">No</th>
                                            <th width="120">No Anggota</th>
                                            <th>Nama Anggota</th>
                                            <th>Wilayah</th>
                                            <th width="50">Status</th>
                                            <th width="230" style="text-align: center;">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($anggota as $key => $value) { ?>
                                        <tr>
                                            <td align="center"><?= $key+1 ?></td>
                                            <td><?= $value->no_kta ?></td>
                                            <td>
                                            <?php if(empty($value->foto)){ ?>
                                                <img src="<?= base_url() ?>/asset/img/default.png" width="40" height="40" class="rounded-circle"> <?= $value->nama_anggota ?>
                                            <?php } else { ?>
                                                <img src="<?= base_url() ?>/asset/img/berkas/<?= $value->foto ?>" width="40" height="40" class="rounded-circle"> <?= $value->nama_anggota ?>
                                            <?php } ?>
                                            </td>
                                            <td><?= $value->wil ?></td>
                                            <td align="center">
                                            <?php if($value->status == 1){
                                                echo '<i class="badge badge-success p-2">Aktif</i>';
                                            }else{
                                                echo '<a href="'.base_url('verif/').$value->no_kta.'"><i class="badge badge-danger p-2">verifikasi</i></a>';
                                            } ?>
                                            </td>
                                            <td>
                                            <a onclick="deleteConfirm('<?= base_url('anggota/delete/'.$value->no_kta) ?>')"
                                             href="#!" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i> Hapus</a>
                                                <a href="<?= base_url() ?>/form-edit/anggota/<?= $value->no_kta ?>" class="btn btn-success btn-sm"><span class="fa fa-edit"></span> Edit</a>
                                                <a href="<?= base_url() ?>anggota/detail/<?= $value->no_kta ?>" class="btn btn-primary btn-sm"><span class="fa fa-eye"></span> Detail</a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
             
                </div>
                <!-- /.container-fluid -->
                <?php $this->load->view('layout/footer.php'); ?>
