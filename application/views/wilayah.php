                <?php  
                    $this->load->view('layout/header.php');
                    $this->load->view('layout/nav.php');
                ?>
                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <div class="card shadow mb-4">
                        <div class="card-header py-3" style="background-color: #2F4F4F">
                            <h6 class="m-2 font-weight-bold text-light">Data Wilayah</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                            <a href="<?php echo base_url() ?>add-wilayah" class="btn btn-danger btn-sm mb-3">Tambah Data </a>
                        <?php if($this->session->flashdata('success')){ ?>
                          <div class="alert alert-success"><?= $this->session->flashdata('success'); ?></div>
                        <?php } ?>

                        <?php if($this->session->flashdata('error')){ ?>
                          <div class="alert alert-danger"><?= $this->session->flashdata('error'); ?></div>
                        <?php } ?>
                                <table class="table table-bordered table-striped" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th width="1">No</th>
                                            <th width="120">Nama Wilayah</th>
                                            <th>Kecamatan</th>
                                            <th>Kabupaten</th>
                                            <th>Provinsi</th>
                                            <th width="140" style="text-align: center;">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($wil as $key => $value) { ?>
                                        <tr>
                                            <td align="center"><?= $key+1 ?></td>
                                            <td><?= $value->wil ?></td>
                                            <td><?= $value->kec ?></td>
                                            <td><?= $value->kab ?></td>
                                            <td><?= $value->prov ?></td>
                                            <td align="center">
                                              <a onclick="deleteConfirm('<?= base_url('wilayah/delete/'.$value->id) ?>')"
                                             href="#!" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i> Hapus</a>
                                              <a href="<?= base_url() ?>form-edit/wilayah/<?= $value->id ?>" class="btn btn-success btn-sm"><span class="fa fa-edit"></span> Edit</a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
             
                </div>
                <!-- /.container-fluid -->
                <?php $this->load->view('layout/footer.php'); ?>
