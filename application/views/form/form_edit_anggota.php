                <div class="container-fluid">
                    <!-- DataTales Example -->
                 
                    <div class="card shadow mb-4">
                        <div class="card-header py-3" style="background-color: #2F4F4F">
                            <h6 class="m-2 font-weight-bold text-light">Form Edit Calon Anggota</h6>
                        </div>
                        <div class="card-body">
                        <div style="text-align: center;">
                            <img src="<?= base_url() ?>/asset/img/cop.png" style="width: 90%; height: 150px;"> 
                        </div>
                        <form class="pl-4 pr-4 ml-5 mr-5" action="<?= base_url('Controller_anggota/update') ?>" method="POST" enctype="multipart/form-data">
                        <hr>
                        <div class="alert alert-primary" role="alert">
                          Data Diri Calon Anggota
                        </div>
                          <div class="form-row">
                            <div class="form-group col-md-6">
                              <label for="inputEmail4">Nama Lengkap</label>
                              <input type="hidden" class="form-control" name="kta" required="" value="<?= $value->no_kta ?>">
                              <input class="form-control" name="nama" required="" value="<?= $value->nama_anggota ?>">
                            </div>
                            <div class="form-group col-md-6">
                              <label for="inputPassword4">No NIK / NIP</label>
                              <input class="form-control" name="nik" value="<?= $value->nik ?>">
                            </div>
                          </div>    
                          <div class="form-row">
                            <div class="form-group col-md-6">
                              <label for="inputEmail4">Tempat Lahir</label>
                              <input class="form-control" name="tempat_lahir" required="" value="<?= $value->tempat_lahir ?>">
                            </div>
                            <div class="form-group col-md-6">
                              <label for="inputPassword4">Tanggal Lahir</label>
                              <input type="date" class="form-control" name="tgl_lahir" required="" value="<?= $value->tgl_lahir ?>">
                            </div>
                          </div>
                          <div class="form-row">
                            <div class="form-group col-md-6">
                              <label for="inputEmail4">Telp / HP</label>
                              <input class="form-control" name="telp" required="" value="<?= $value->telp ?>">
                            </div>
                            <div class="form-group col-md-6">
                              <label for="inputPassword4">Email</label>
                              <input class="form-control" name="email" required="" value="<?= $value->email ?>">
                            </div>
                          </div>    
                          <div class="form-row">
                            <div class="form-group col-md-6">
                              <label for="inputEmail4">Jenis Kelamin</label>
                              <select class="custom-select" name="jk" required="">
                                  <option <?php if($value->jk == 'Laki - Laki'){ echo "selected"; }?>>Laki - Laki</option>
                                  <option <?php if($value->jk == 'Perempuan'){ echo "selected"; }?> >Perempuan</option>
                              </select>
                            </div>
                            <div class="form-group col-md-6">
                              <label for="inputPassword4">Permohono Anggota Untuk Wilayah</label>
                              <select class="custom-select" name="wilayah" required="">
                                <?php foreach ($wil as $v) { ?>
                                <option value="<?= $v->id ?>" <?php if($v->id == $value->wilayah){ echo "selected"; } ?>><?= $v->wil ?></option>
                                <?php } ?>

                              </select>
                            </div>
                          </div>                          
                          <div class="form-group">
                            <label for="inputAddress">Alamat</label>
                            <textarea class="form-control" rows="3" name="alamat" required=""><?= $value->alamat ?></textarea>
                          </div>
                          <div class="form-group">
                            <label for="inputAddress">Status Keanggotaan</label>
                            <select class="custom-select" name="stat">
                                <option <?php if($value->status == 1){echo "selected";} ?> value="1">Set sebagai anggota aktif</option>
                                <option <?php if($value->status == 0){echo "selected";} ?> value="0">Set sebagai anggota non aktif</option>
                            </select>
                          </div>
                          <div class="form-row">
                            <div class="form-group col">
                              <label for="inputCity">Kelurahan / Desa</label>
                              <input class="form-control" required="" name="kelurahan" value="<?= $value->desa ?>">
                            </div>
                            <div class="form-group col">
                              <label for="inputCity">Kecamatan</label>
                              <input class="form-control" name="kec" required="" value="<?= $value->kec ?>">
                            </div>
                            <div class="form-group col">
                              <label for="inputCity">Kabupaten</label>
                              <input class="form-control" name="kab" required="" value="<?= $value->kab ?>">
                            </div>
                            <div class="form-group col">
                              <label for="inputCity">Provinsi</label>
                              <input class="form-control" name="prov" type="text" value="<?= $value->prov ?>">
                            </div>
                            <div class="form-group col-md-2">
                              <label for="inputZip">Kode Pos</label>
                              <input class="form-control" name="pos" required="" value="<?= $value->pos ?>">
                            </div>
                          </div>
                                </tbody>
                            </table>
                          <button type="submit" class="btn btn-primary">Simpan Data</button>
                        </form><hr>
                        </div>
                    </div>