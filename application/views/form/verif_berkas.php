                <div class="container-fluid">
                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3" style="background-color: #2F4F4F">
                            <h6 class="m-2 font-weight-bold text-light">Verifikasi Berkas Pendaftaran Anggota</h6>
                        </div>
                        <div class="card-body">
                        <div style="text-align: center;">
                            <img src="<?= base_url() ?>/asset/img/cop.png" style="width: 90%; height: 150px;"> 
                        </div>
                        <hr>
                        <?php if($this->session->flashdata('success')){ ?>
                          <div class="alert alert-success"><?= $this->session->flashdata('success'); ?></div>
                        <?php } ?>

                        <?php if($this->session->flashdata('error')){ ?>
                          <div class="alert alert-danger"><?= $this->session->flashdata('error'); ?></div>
                        <?php } ?>

<div class="alert alert-danger">
<pre>Ketereangan
Max Size File : 2 MB
Format File   : jpg/jpeg/png/</pre>
</div>

                        <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Nama Berkas</th>
                                        <th>Berkas</th>
                                        <th>Keterangan</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>File KTP</td>
                                        <form action="<?=base_url('Controller_anggota/uploadKtp/'.$id.'')?>" method="post" enctype="multipart/form-data">
                                         <?php if(empty($ktp->berkas)){ 
                                          echo '<td><input type="file" name="ktp"></td>';
                                          echo '<td>File belum diupload</td>';
                                          echo '<td><input type="submit" class="btn btn-primary" value="Upload"></td>';
                                         }else{
                                          echo '<td><a href="'.base_url('asset/img/berkas/').$ktp->berkas.'" class="btn btn-danger btn-sm" target="_blank">lihat File<a/></td>';
                                          echo '<td>'.$ktp->ket.'</td>';
                                          echo '<td><a href="'.base_url('verif/delete/'.$id).'" class="btn btn-danger">Hapus</a></td>';

                                         } ?>
                                        </form>
                                    </tr>
                                    <tr>
                                        <form action="<?=base_url('Controller_anggota/uploadKK/'.$id.'')?>" method="post" enctype="multipart/form-data">
                                        <td>File Kartu Keluarga</td>
                                        <?php if(empty($kk->berkas)){
                                        echo ' 
                                        <td><input type="file" name="kk"></td>
                                        <td>File belum diupload</td>
                                        <td><input type="submit" class="btn btn-primary" value="Upload"></td>
                                        ';
                                         }else{
                                          echo '<td><a href="'.base_url('asset/img/berkas/').$kk->berkas.'" class="btn btn-danger btn-sm">lihat File<a/></td>';
                                          echo '<td>'.$kk->ket.'</td>';
                                          echo '<td><a href="'.base_url('verif/deletekk/'.$id).'" class="btn btn-danger">Hapus</a></td>';
                                         } ?>
                                    </form>
                                    </tr>
                                    <tr>
                                        <form action="<?=base_url('Controller_anggota/uploadFoto/'.$id.'')?>" method="post" enctype="multipart/form-data">

                                        <td>Foto Latar Belakang Merah</td>
                                        <?php if(empty($foto->berkas)){
                                          echo '
                                            <td><input type="file" name="foto"></td>
                                            <td>File belum diupload</td>
                                            <td><input type="submit" class="btn btn-primary" value="Upload"></td>
                                          ';
                                         }else{
                                          echo '<td><a href="'.base_url('asset/img/berkas/').$foto->berkas.'" class="btn btn-danger btn-sm">lihat File<a/></td>';
                                          echo '<td>'.$foto->ket.'</td>';
                                          echo '<td><a href="'.base_url('verif/deletefoto/'.$id).'" class="btn btn-danger">Hapus</a></td>';
                                         } ?>
                                    </form>
                                    </tr>
                                    <tr>
                                        <form action="<?=base_url('Controller_anggota/uploadBukti/'.$id.'')?>" method="post" enctype="multipart/form-data">
                                        <td>Bukti Pembayaran</td>
                                        <?php if(empty($bukti->berkas)){
                                          echo '
                                            <td><input type="file" name="bukti"></td>
                                            <td>File belum diupload</td>
                                            <td><input type="submit" class="btn btn-primary" value="Upload"></td>
                                          ';
                                         }else{
                                          echo '<td><a href="'.base_url('asset/img/berkas/').$bukti->berkas.'" class="btn btn-danger btn-sm">lihat File<a/></td>';
                                          echo '<td>'.$bukti->ket.'</td>';
                                          echo '<td><a href="'.base_url('verif/deletebukti/'.$id).'" class="btn btn-danger">Hapus</a></td>';
                                         } ?>

                                    </form>
                                    </tr>
                                </tbody>
                            </table><hr>
                        <?php if($_SESSION['akses'] == 'Adminstrator'){ ?>    
                          <a href="<?= base_url('anggota') ?>" class="btn btn-danger">Kembali</a>
                        <?php }else { ?>
                          <a href="<?= base_url('anggota/detail/'.$_SESSION
                          ['ses_id'].'') ?>" class="btn btn-danger">Kembali</a>
                        <?php } ?>
                        </div>
                        </div>
                    </div>