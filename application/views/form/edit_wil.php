            <?php $this->load->view('layout/header.php'); ?>
            <?php $this->load->view('layout/nav.php'); ?>
            <div class="container-fluid">
            <!-- DataTales Example -->
            <div class="card shadow mb-4">
                <div class="card-header py-3" style="background-color: #2F4F4F">
                    <h6 class="m-2 font-weight-bold text-light">Form Edit Wilayah Cab PAO</h6>
                </div>
                <div class="card-body">
                <div style="text-align: center;">
                    <img src="<?= base_url() ?>/asset/img/cop.png" style="width: 90%; height: 150px;"> 
                </div><hr>
                <form class="m-5" action="<?php echo base_url('Controller_wil/update_wil') ?>" method="post">
                  <div class="form-group row">
                    <label for="staticEmail" class="col-sm-3 col-form-label">Masukkan Nama Wilayah Cab PAO</label>
                    <div class="col-sm-8">
                      <input type="hidden" class="form-control" name="id" required="" value="<?= $wil->id ?>">
                      <input type="text" class="form-control" name="wil" required="" value="<?= $wil->wil ?>">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="staticEmail" class="col-sm-3 col-form-label">Kecamatan</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="kec" required="" value="<?= $wil->kec ?>">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="staticEmail" class="col-sm-3 col-form-label">Kabupaten</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="kab" required="" value="<?= $wil->kab ?>">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="staticEmail" class="col-sm-3 col-form-label">Provinsi</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="prov" required="" value="<?= $wil->prov ?>">
                    </div>
                  </div>
                    <hr>
                    <input type="submit" class="btn btn-primary" value="Simpan Data" name="">
                </form>
                </div>
            </div> 
            <?php $this->load->view('layout/footer.php'); ?>