                <div class="container-fluid">
                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3" style="background-color: #2F4F4F">
                            <h6 class="m-2 font-weight-bold text-light">Form Pendataan Calon Anggota</h6>
                        </div>
                        <div class="card-body">
                        <div style="text-align: center;">
                            <img src="<?= base_url() ?>/asset/img/cop.png" style="width: 90%; height: 150px;"> 
                        </div>
                        <form class="pl-4 pr-4 ml-5 mr-5" action="<?= base_url('Controller_anggota/save') ?>" method="POST" enctype="multipart/form-data">
                        <hr>
                        <div class="alert alert-primary" role="alert">
                          Data Diri Calon Anggota
                        </div>
                          <div class="form-row">
                            <div class="form-group col-md-6">
                              <label for="inputEmail4">Nama Lengkap</label>
                              <input class="form-control" name="nama" required="">
                            </div>
                            <div class="form-group col-md-6">
                              <label for="inputPassword4">No NIK / NIP</label>
                              <input class="form-control" name="nik" required="">
                            </div>
                          </div>    
                          <div class="form-row">
                            <div class="form-group col-md-6">
                              <label for="inputEmail4">Tempat Lahir</label>
                              <input class="form-control" name="tempat_lahir" required="">
                            </div>
                            <div class="form-group col-md-6">
                              <label for="inputPassword4">Tanggal Lahir</label>
                              <input type="date" class="form-control" name="tgl_lahir" required="">
                            </div>
                          </div>
                          <div class="form-row">
                            <div class="form-group col-md-6">
                              <label for="inputEmail4">Telp / HP</label>
                              <input class="form-control" name="telp" required="">
                            </div>
                            <div class="form-group col-md-6">
                              <label for="inputPassword4">Email</label>
                              <input class="form-control" name="email" required="">
                            </div>
                          </div>    
                          <div class="form-row">
                            <div class="form-group col-md-6">
                              <label for="inputEmail4">Jenis Kelamin</label>
                              <select class="custom-select" name="jk" required="">
                                  <option>...</option>
                                  <option>Laki - Laki</option>
                                  <option>Perempuan</option>
                              </select>
                            </div>
                            <div class="form-group col-md-6">
                              <label for="inputPassword4">Permohono Anggota Untuk Wilayah</label>
                              <select class="custom-select" name="wilayah" required="">
                                <option selected="" disabled="">...</option>
                                <?php foreach ($wil as $key => $value) { ?>
                                <option value="<?= $value->id ?>"><?= $value->wil ?></option>
                                <?php } ?>

                              </select>
                            </div>
                          </div>                          
                          <div class="form-group">
                            <label for="inputAddress">Alamat</label>
                            <textarea class="form-control" rows="3" name="alamat" required=""></textarea>
                          </div>
                          <div class="form-row">
                            <div class="form-group col">
                              <label for="inputCity">Kelurahan / Desa</label>
                              <input class="form-control" required="" name="kelurahan">
                            </div>
                            <div class="form-group col">
                              <label for="inputCity">Kecamatan</label>
                              <input class="form-control" name="kec" required="">
                            </div>
                            <div class="form-group col">
                              <label for="inputCity">Kabupaten</label>
                              <input class="form-control" name="kab" required="">
                            </div>
                            <div class="form-group col">
                              <label for="inputCity">Provinsi</label>
                              <input class="form-control" name="prov" type="text">
                            </div>
                            <div class="form-group col-md-2">
                              <label for="inputZip">Kode Pos</label>
                              <input class="form-control" name="pos" required="">
                            </div>
                          </div>
                        <div class="alert alert-primary" role="alert">
                          Riwayat Organisasi
                        </div>
                            <table class="table table-hover" id="dinamis">
                                <thead>
                                    <tr>
                                        <th width="570">Nama Organisasi</th>
                                        <th>Jabatan</th>
                                        <th>Priode</th>
                                        <th>
                                            <button type="button" class="btn btn-blue add-row">+</button></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><input class="form-control" type="text" name="nama_or[]"></td>
                                        <td>
                                            <select class="custom-select" name="jabatan[]">
                                                <option>...</option>
                                                <option>Anggota</option>
                                                <option>Pengurus</option>
                                            </select>
                                        </td>
                                        <td><input class="form-control" type="text" name="periode[]"></td>
                                        <td>
                                            <button type="button" class="btn btn-danger delete-row">-</button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                          Saya mohon untuk menjadi Anggota Organisasi Pallapi Arona Ogi’e (PAO wilayah :
                          <hr><div class="form-group">
                            <div class="form-check">
                              <input class="form-check-input" type="checkbox" id="check1" required="">
                              <label class="form-check-label" for="check1">
                                Saya bersedia mematuhi AD/ART dan seluruh peraturan yang berlaku di Organisasi <b style="color: red">Pallapi Arona Ogi’e (PAO)</b>
                              </label>
                            </div>
                            <div class="form-check">
                              <input class="form-check-input" type="checkbox" id="check2" required="">
                              <label class="form-check-label" for="check2">
                                Saya akan berperilaku baik, jujur, bertanggung jawab, adil, dan mempunyai integritas yang tinggi
                              </label>
                            </div>
                            <div class="form-check">
                              <input class="form-check-input" type="checkbox" id="check3">
                              <label class="form-check-label" for="check3">
                                Pernyataan tersebut di atas saya buat dengan sebenar-benarnya dan dapat dipertanggung jawabkan.
                              </label>
                            </div>
                          </div>
                          <button type="submit" class="btn btn-primary">Simpan Data</button>
                        </form><hr>
                        <div class="ml-5 mr-5">
                        Catatan :
                        <div class="alert alert-danger" role="alert">
                            Biaya pembuatan KTA Rp.30.000,- (Tiga Puluh Ribu Rupiah), transfer ke Rekening No._______________________ atas nama Organisasi Pallapi Arona Ogi’e (PAO)
                        </div>
                        </div>
                        </div>
                    </div>