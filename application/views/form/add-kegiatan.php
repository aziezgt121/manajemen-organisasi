            <?php $this->load->view('layout/header.php'); ?>
            <?php $this->load->view('layout/nav.php'); ?>
            <div class="container-fluid">
            <!-- DataTales Example -->
            <div class="card shadow mb-4">
                <div class="card-header py-3" style="background-color: #2F4F4F">
                    <h6 class="m-2 font-weight-bold text-light">Form Pendataan Kegiatan PAO</h6>
                </div>
                <div class="card-body">
                <div style="text-align: center;">
                    <img src="<?= base_url() ?>/asset/img/cop.png" style="width: 90%; height: 150px;"> 
                </div><hr>
                <?php echo form_open_multipart('Controller_kegiatan/save'); ?>
                  <div class="form-group row">
                    <label for="staticEmail" class="col-sm-3 col-form-label">Nama Kegiatan</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="nama" required="">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="staticEmail" class="col-sm-3 col-form-label">Waktu / Jadwal Kegiatan</label>
                    <div class="col-sm-8">
                    <div class="row">
                      <div class="col">
                        <input type="date" class="form-control" name="tgl" required="" placeholder="Tanggal">
                      </div>
                      <div class="col">
                        <input type="time" class="form-control" name="jam" required="" placeholder="Jam">
                      </div>
                    </div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="staticEmail" class="col-sm-3 col-form-label">Lokasi Kegiatan</label>
                    <div class="col-sm-8">
                    <div class="row">
                      <div class="col">
                        <input type="text" class="form-control" name="desa" required="" placeholder="Kelurahan / Desa">
                      </div>
                      <div class="col">
                        <input type="text" class="form-control" name="kec" required="" placeholder="Kecamatan">
                      </div>
                      <div class="col">
                        <input type="text" class="form-control" name="kab" required="" placeholder="Kabupaten">
                      </div>
                    </div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="staticEmail" class="col-sm-3 col-form-label">Alamat Kegiatan</label>
                    <div class="col-sm-8">
                    <textarea class="form-control" cols="5" name="alamat" rows="3"></textarea>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="staticEmail" class="col-sm-3 col-form-label">Masukkan Nama Penanggung Jawab</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="pj" required="">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="staticEmail" class="col-sm-3 col-form-label">Upload Dokumen Kegiatan</label>
                    <div class="col-sm-8">
                      <input type="file" class="" name="documen">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="staticEmail" class="col-sm-3 col-form-label">Catatan</label>
                    <div class="col-sm-8">
                    <textarea class="form-control" cols="5" rows="5" name="ket"></textarea>
                    </div>
                  </div>
                    <hr>
                    <input type="submit" class="btn btn-primary" value="Simpan Data" name="">
                </form>
                </div>
            </div> 
            <?php $this->load->view('layout/footer.php'); ?>