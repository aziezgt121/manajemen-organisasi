                <?php  
                    $this->load->view('layout/header.php');
                    $this->load->view('layout/nav.php');
                ?>
                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <!-- DataTales Example -->
                    <?php if ($this->session->flashdata('category_success')) { ?>
                        <div class="alert alert-success"> <?= $this->session->flashdata('category_success') ?> </div>
                    <?php } ?>

                    <?php if ($this->session->flashdata('category_error')) { ?>
                        <div class="alert alert-danger"> <?= $this->session->flashdata('category_error') ?> </div>
                    <?php } ?>
                    <div class="card shadow mb-4">
                        <div class="card-header py-3" style="background-color: #2F4F4F">
                            <h6 class="m-2 font-weight-bold text-light">Data Kegiatan</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                            <?php if(isset($_SESSION['akses']) == 'Adminstrator'){ ?>
                            <a href="<?php echo base_url() ?>add-kegiatan" class="btn btn-danger btn-sm mb-3">Tambah Data</a>
                            <?php } ?>
                        <?php if($this->session->flashdata('success')){ ?>
                          <div class="alert alert-success"><?= $this->session->flashdata('success'); ?></div>
                        <?php } ?>

                        <?php if($this->session->flashdata('error')){ ?>
                          <div class="alert alert-danger"><?= $this->session->flashdata('error'); ?></div>
                        <?php } ?>
                                <table class="table table-bordered table-striped" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th width="1">No</th>
                                            <th width="120">Nama Kegiatan</th>
                                            <th>Jadwal</th>
                                            <th>Lokasi</th>
                                            <th>Alamat</th>
                                            <th>Berkas</th>
                                            <?php if(isset($_SESSION['akses']) == 'Adminstrator'){ 
                                                echo '<th width="130" style="text-align: center;">Action</th>';
                                            } ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($kegiatan as $key => $value) { ?>
                                        <tr>
                                            <td align="center"><?= $key+1 ?></td>
                                            <td><?= $value->nama ?></td>
                                            <td><?= date("d F Y : H:i", strtotime($value->jadwal)); ?></td>
                                            <td><?= $value->desa.', '.$value->kec ?></td>
                                            <td><?= $value->alamat ?></td>
                                            <td><a href="<?= base_url('asset/img/berkas/doc_kegiatan/'.$value->file.''); ?>" target="_blank">Lihat File</a></td>
                                            <?php if(isset($_SESSION['akses']) == 'Adminstrator'){ ?>
                                            <td align="center">
                                            <a onclick="deleteConfirm('<?= base_url('kegiatan/delete/'.$value->id) ?>')"
                                             href="#!" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i> Hapus</a>
                                                <a href="<?= base_url() ?>form-edit/kegiatan/<?= $value->id ?>" class="btn btn-success btn-sm"><span class="fa fa-edit"></span> Edit</a>
                                            </td>
                                            <?php } ?>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
             
                </div>
                <!-- /.container-fluid -->
                <?php $this->load->view('layout/footer.php'); ?>
