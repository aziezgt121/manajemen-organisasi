                <?php
                    $this->load->view('layout/header.php');
                    $this->load->view('layout/nav.php');
                ?>
                        <div class="container-fluid">

                        <!-- DataTales Example -->
                        <div class="card shadow mb-4">
                            <div class="card-header py-3" style="background-color: #2F4F4F">
                                <h6 class="m-2 font-weight-bold text-light">Laporan Aktifitas</h6>
                            </div>
                            <div class="card-body">
                            <div style="text-align: center;">
                            <?php if(isset($_GET['action'])){ ?>
                                <img src="<?= base_url() ?>/asset/img/cop.png" style="width: 90%; height: 150px;"><hr> 
                                <h3 style="font-weight: bold; color: black">Laporan Data Kegiatan</h3>
                                <p style="font-size: 18px;margin : 11px; ">Periode : <?= date('d F Y', strtotime($_GET['awal'])) ." - ". date('d F Y', strtotime($_GET['akhir'])) ?></p>

                                <table class="table table-bordered table-striped" id="" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Kegiatan</th>
                                            <th>Jadwal</th>
                                            <th>Lokasi</th>
                                            <th>Penanggung Jawab</th>
                                            <th>Keterangan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($rep as $key => $vi) { ?>
                                    <tr>
                                        <td><?= $key+1 ?></td>
                                        <td align="left"><?= $vi->nama ?></td>
                                        <td align="left"><?= date('d F Y', strtotime($vi->jadwal)) ?></td>
                                        <td align="left"><?= $vi->alamat.", ".$vi->desa.", ".$vi->kec.", ".$vi->kab ?></td>
                                        <td align="left"><?= $vi->pj ?></td>
                                        <td align="left"><?= $vi->catatan ?></td>
                                    </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            <a href="<?= base_url('report-activity') ?>" class="btn btn-danger">Reset</a>
                            <a href="<?= base_url('cetak-kegiatan?awal='.$_GET["awal"]."&&akhir=".$_GET["akhir"].'') ?>" class="btn btn-primary">Cetak Laporan</a>
                            <?php if(isset($_GET['wil'])){ ?>
                            <a href="" class="btn btn-primary" target="_blank">Cetak Laporan</a>
                            <?php } ?>
                            <!-- <a href="12" class="btn btn-success">Export PDF</a> -->
                                <?php }elseif(isset($_GET['filter'])){ ?>
                                    <form action="" method="get">
                                    <h3 class="pl-3" style="font-weight: bold; color: black" align="left">Filter Data :</h3><br>
                                      <div class="row pl-3 pb-3">
                                        <div class="col col-4"> 
                                        <input type="date" name="awal" class="form-control">
                                        </div>
                                        <div class="col col-4"> 
                                        <input type="date" name="akhir" class="form-control">
                                        </div>
                                        <div class="col col-2">
                                            <input type="submit" name="action" class="btn btn-danger" value="Cari Data">
                                        </div>
                                      </div><hr>
                                    </form>
                                <?php }else{ ?>
                                <img src="<?= base_url() ?>/asset/img/cop.png" style="width: 90%; height: 150px;"><hr> 
                                <h3 style="font-weight: bold; color: black">Laporan Data Kegiatan</h3>
                                <table class="table table-bordered table-striped" id="" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Kegiatan</th>
                                            <th>Jadwal</th>
                                            <th>Lokasi</th>
                                            <th>Penanggung Jawab</th>
                                            <th>Keterangan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($rep as $key => $v) { ?>
                                    <tr>
                                        <td><?= $key+1 ?></td>
                                        <td align="left"><?= $v->nama ?></td>
                                        <td align="left"><?= date('d F Y', strtotime($v->jadwal)) ?></td>
                                        <td align="left"><?= $v->alamat.", ".$v->desa.", ".$v->kec.", ".$v->kab ?></td>
                                        <td align="left"><?= $v->pj ?></td>
                                        <td align="left"><?= $v->catatan ?></td>
                                    </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            <a href="<?= base_url('cetak-kegiatan') ?>" class="btn btn-primary" target="_blank">Cetak Laporan</a>
                            <a href="<?php base_url() ?>report-activity?filter" class="btn btn-danger">Filter Berdasarkan Tanggal</a>
                            <?php if(isset($_GET['wil'])){ ?>
                            <a href="<?= base_url('cetak-kegiatan') ?>" class="btn btn-primary" target="_blank">Cetak Laporan</a>
                            <?php } }?>
                            </div>
                        </div>
                    </div>
             
                </div>
                <!-- /.container-fluid -->
                <?php $this->load->view('layout/footer.php'); ?>
