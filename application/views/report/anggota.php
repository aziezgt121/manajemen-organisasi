                <?php
                    $this->load->view('layout/header.php');
                    $this->load->view('layout/nav.php');
                ?>
                        <div class="container-fluid">

                        <!-- DataTales Example -->
                        <div class="card shadow mb-4">
                            <div class="card-header py-3" style="background-color: #2F4F4F">
                                <h6 class="m-2 font-weight-bold text-light">Laporan Anggota</h6>
                            </div>
                            <div class="card-body">
                            <div style="text-align: center;">
                            <?php if(isset($_GET['action'])){ ?>
                                <img src="<?= base_url() ?>/asset/img/cop.png" style="width: 90%; height: 150px;"><hr> 

                                <h3 style="font-weight: bold; color: black">Laporan Data Anggota</h3>
                                <h5 style="font-weight: bold; color: black">Cab / Wilayah : <?= $nw->wil ?></h5>
                                <table class="table table-bordered table-striped" id="" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th width="1">No</th>
                                            <th width="120">No Anggota</th>
                                            <th>Nama Anggota</th>
                                            <th>Tempat / Tgl Lahir</th>
                                            <th>Telp</th>
                                            <th>Tanggal Gabung</th>
                                            <th>Wilayah</th>
                                            <th>Status Anggota</th>
                                            <th>Alamat</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($anggota as $key => $value) { ?>
                                        <tr style="text-transform: capitalize">
                                            <td align="center"><?= $key+1 ?></td>
                                            <td><?= $value->no_kta ?></td>
                                            <td align="left">
                                            <?php if(empty($value->foto)){ ?>
                                                <img src="<?= base_url() ?>/asset/img/default.png" width="40" height="40" class="rounded-circle"> <?= $value->nama_anggota ?>
                                            <?php } else { ?>
                                                <img src="<?= base_url() ?>/asset/img/berkas/<?= $value->foto ?>" width="40" height="40" class="rounded-circle"> <?= $value->nama_anggota ?>
                                            <?php } ?>
                                            </td>
                                            <td align="left"><?= $value->tempat_lahir." / ".date('d F Y', strtotime($value->tgl_lahir)) ?></td>
                                            <td><?= $value->telp ?></td>
                                            <td><?= $value->tgl_gabung ?></td>
                                            <td><?= $value->wil ?></td>
                                            <td align="center">
                                            <?php if($value->status == 1){
                                                echo '<i class="badge badge-success p-2">Aktif</i>';
                                            }else{
                                                echo '<a href="'.base_url('verif/').$value->no_kta.'"><i p-2">Tidak Aktif</i></a>';
                                            } ?>
                                            </td>
                                            <td><?= $value->alamat ?></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            <a href="<?= base_url('report-member') ?>" class="btn btn-danger">Reset</a>
                            <?php if(isset($_GET['wil'])){ ?>
                            <a href="<?= base_url('cetak-member/'.$_GET['wil']) ?>" class="btn btn-primary" target="_blank">Cetak Laporan</a>
                            <?php } ?>
                            <!-- <a href="12" class="btn btn-success">Export PDF</a> -->
                                <?php }else{ ?>
                                    <form action="" method="get">
                                    <h3 class="pl-3" style="font-weight: bold; color: black" align="left">Filter Data :</h3><br>
                                      <div class="row pl-3 pb-3">
                                        <div class="col col-6"> 
                                            <select class="custom-select" name="wil">
                                                <option selected="" disabled="">Pilih Lokasi / Wilayah</option>
                                                <?php foreach ($wil as $key => $v) {
                                                    echo '<option value="'.$v->id.'">'.$v->wil.'</option>';
                                                } ?>
                                            </select>
                                        </div>
                                        <div class="col col-2">
                                            <input type="submit" name="action" class="btn btn-danger" value="Cari Data">
                                        </div>
                                      </div><hr>
                                    </form>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
             
                </div>
                <!-- /.container-fluid -->
                <?php $this->load->view('layout/footer.php'); ?>
