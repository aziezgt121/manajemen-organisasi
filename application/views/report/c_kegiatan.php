<!DOCTYPE html>
<html>
<head>
	<title>Laporan Anggota</title>
</head>
<body>
	<img src="<?= base_url() ?>/asset/img/cop.png" style="width: 100%; height: 150px;"><hr> 
	<h3 style="font-size: 24px;margin : 11px; ">Laporan Laporan Kegiatan</h3>
	<?php if(isset($_GET['awal'])){ ?>
	<p style="font-size: 18px;margin : 11px; ">Periode : <?= date('d F Y', strtotime($_GET['awal'])) ?> - <?= date('d F Y', strtotime($_GET['akhir'])) ?></p>
	<?php } ?>
	<table border="1" style="border-collapse: collapse; margin : 11px; width: 99%; padding: 10px;">
        <tr>
            <th>No</th>
            <th style="text-align: left;">Kegiatan</th>
            <th style="text-align: left;">Jadwal</th>
            <th style="text-align: left;">Lokasi</th>
            <th style="text-align: left;">Penanggung Jawab</th>
            <th style="text-align: left;">Keterangan</th>
        </tr>
        <?php if(isset($_GET['awal'])){ ?>
        <?php foreach ($rep as $key => $vi) { ?>
        <tr>
            <td><?= $key+1 ?></td>
            <td align="left"><?= $vi->nama ?></td>
            <td align="left"><?= date('d F Y', strtotime($vi->jadwal)) ?></td>
            <td align="left"><?= $vi->alamat.", ".$vi->desa.", ".$vi->kec.", ".$vi->kab ?></td>
            <td align="left"><?= $vi->pj ?></td>
            <td align="left"><?= $vi->catatan ?></td>
        </tr>
        <?php } }else{ ?>
       <?php foreach ($rep as $key => $v) { ?>
        <tr>
            <td><?= $key+1 ?></td>
            <td align="left"><?= $v->nama ?></td>
            <td align="left"><?= date('d F Y', strtotime($v->jadwal)) ?></td>
            <td align="left"><?= $v->alamat.", ".$v->desa.", ".$v->kec.", ".$v->kab ?></td>
            <td align="left"><?= $v->pj ?></td>
            <td align="left"><?= $v->catatan ?></td>
        </tr>
        <?php } ?>
        <?php } ?>
	</table>
	<p align="right">Tembilahan, <?= date('d F Y', strtotime(date('y-m-d'))); ?><p>
<script type="text/javascript">
	window.print();
</script>
</body>
</html>