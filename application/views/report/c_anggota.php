<!DOCTYPE html>
<html>
<head>
	<title>Laporan Anggota</title>
</head>
<body>
	<img src="<?= base_url() ?>/asset/img/cop.png" style="width: 100%; height: 150px;"><hr> 
	<h3 style="font-size: 24px;margin : 11px; ">Laporan Data Anggota</h3>
	<p style="font-size: 18px;margin : 11px; ">Wilayah : <?= $nw->wil ?></p>
	<p style="font-size: 18px;margin : 11px; ">Periode : <?= date('d F Y', strtotime(date('y-m-d'))) ?></p>
	<table border="1" style="border-collapse: collapse; margin : 11px; width: 99%; padding: 10px;">
		<tr>
			<th width="40">No</th>
			<th style="width: auto;" ">No Anggota</th>
			<th style="width: auto;" ">Nama Anggota</th>
			<th style="width: auto;" ">Tempat / Tanggal Lahir</th>
			<th style="width: auto;" ">Telp</th>
			<th style="width: auto;" ">Email</th>
			<th style="width: auto;" ">Status</th>
			<th style="width: auto;" ">Alamat</th>
			<th>Tanggal Gabung</th>
		</tr>
		<?php foreach ($anggota as $key => $v) { ?>
		<tr style="text-transform: capitalize">
			<td align="center"><?= $key+1; ?></td>
			<td><?= $v->no_kta ?></td>
			<td><?= $v->nama_anggota ?></td>
            <td align="left"><?= $v->tempat_lahir." / ".date('d F Y', strtotime($v->tgl_lahir)) ?></td>
            <td><?= $v->telp ?></td>
            <td><?= $v->email ?></td>
            <td align="center">
            <?php if($v->status == 1){
                echo '<i class="badge badge-success p-2">Aktif</i>';
            }else{
                echo '<a href="'.base_url('verif/').$v->no_kta.'"><i p-2">Tidak Aktif</i></a>';
            } ?>
            </td>
            <td><?= $v->alamat.', '.$v->desa.', '.$v->kec.', '.$v->kab.', '.$v->prov ?></td>
            <td><?= $v->tgl_gabung ?></td>
		</tr>
		<?php } ?>
	</table>
	<p align="right">Tembilahan, <?= date('d F Y', strtotime(date('y-m-d'))); ?><p>
<script type="text/javascript">
	window.print();
</script>
</body>
</html>