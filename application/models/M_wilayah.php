<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_wilayah extends CI_Model {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	private $_table = "tb_wilayah";

    public $id;
    public $wil;
    public $kec;
    public $kab;
    public $prov;


    public function getAll()
    {
    	return $this->db->get($this->_table)->result();
    }
 	public function getById($id)
    {
        return $this->db->get_where($this->_table, ["id" => $id])->row();
    }

    public function save()
    {
        $post = $this->input->post();
        $this->wil = $post["wil"];
        $this->kec = $post["kec"];
        $this->kab = $post["kab"];
        $this->prov = $post["prov"];

        return $this->db->insert($this->_table, $this);
    }

    public function update()
    {
        $post = $this->input->post();
        $this->id = $post["id"];
        $this->wil = $post["wil"];
        $this->kec = $post["kec"];
        $this->kab = $post["kab"];
        $this->prov = $post["prov"];

        return $this->db->update($this->_table, $this, array('id' => $post['id']));
    }

    public function destroy($id)
    {
        return $this->db->delete('tb_wilayah', array('id'=>$id));

    }	

}
