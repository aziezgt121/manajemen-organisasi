<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_kegiatan extends CI_Model {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function getAll()
	{
		return $this->db->order_by('jadwal', 'DESC')->get('tb_kegiatan')->result();
	}
    function getById($id){
        return $this->db->get_where('tb_kegiatan', array('id'=>$id))->row();
    }
    function getberkas($id){
        $query=$this->db->query("SELECT * FROM det_anggota WHERE id_anggota = '$id' ");
        return $query->result();
    }
    function save($file_element_name)
    {
         $data = array(
            'id'      =>   abs(crc32(uniqid())),
            'nama'      => $this->input->post('nama'),
            'jadwal'      => $this->input->post('tgl')." ".$this->input->post('jam'),
            'desa'      => $this->input->post('desa'),
            'kec'      => $this->input->post('kec'),
            'kab'      => $this->input->post('kab'),
            'alamat'      => $this->input->post('alamat'),
            'pj'      => $this->input->post('pj'),
            'file'      => $file_element_name,
            'catatan'      => $this->input->post('ket')
        );
        $this->db->insert('tb_kegiatan', $data);
    }
    function update($f_name)
    {
         $data = array(
            'nama'    => $this->input->post('nama'),
            'jadwal'  => $this->input->post('tgl')." ".$this->input->post('jam'),
            'desa'    => $this->input->post('desa'),
            'kec'     => $this->input->post('kec'),
            'kab'     => $this->input->post('kab'),
            'alamat'  => $this->input->post('alamat'),
            'pj'      => $this->input->post('pj'),
            'file'    => $f_name,
            'catatan' => $this->input->post('ket')
        );
        $this->db->update('tb_kegiatan', $data, array('id' => $this->input->post('id')));
    }
    function destroy($id)
    {
        return $this->db->delete('tb_kegiatan', array('id'=>$id));
    }
    function getBy($awal, $akhir)
    {
		return $this->db->query("SELECT * FROM tb_kegiatan WHERE jadwal BETWEEN '$awal' AND '$akhir' ORDER BY jadwal Desc ")->result();
    }

}
