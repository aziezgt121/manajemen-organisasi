<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_anggota extends CI_Model {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */



	private $_table = "tb_anggota";

    public $id;
    public $no_kta;
    public $foto;
    public $nama_anggota;
    public $tempat_lahir;
    public $tgl_lahir;
    public $wilayah;
    public $telp;
    public $email;
    public $nik;
    public $alamat;
    public $desa;   
    public $kec;   
    public $kab;   
    public $prov;   
    public $isdelete;   
    public $jk;   
    public $pos;   
    public $status;   

    public function save($data,$table){

        $this->db->insert($table,$data);
    }
    public function getAll()
    {
    	$this->db->select('*');
        $this->db->from('tb_anggota');
        $this->db->join('tb_wilayah', 'tb_anggota.wilayah = tb_wilayah.id');
        $this->db->where('isdelete', 1);
        
        $query = $this->db->get();


        return $query->result();

    }
    public function getByWil($id)
    {
        $this->db->select('*');
        $this->db->from('tb_anggota');
        $this->db->join('tb_wilayah', 'tb_anggota.wilayah = tb_wilayah.id');
        $this->db->where('wilayah', $id);
        
        $query = $this->db->get();


        return $query->result();

    }
    public function getAllById($id)
    {
        $this->db->select('*');
        $this->db->from('tb_anggota');
        $this->db->join('tb_wilayah', 'tb_anggota.wilayah = tb_wilayah.id');
        $this->db->where('wilayah', $id);
        
        $query = $this->db->get();


        return $query->result();

    }
    public function get_berkas($value='')
    {
        return $This->db->get_where('tb_anggota', array('id_anggota' => $id ));
    }
 	public function getById($id)
    {
        return $this->db->get_where('tb_anggota', array('no_kta' => $id))->row();
    }
    public function getKTP($id)
    {
        return $this->db->get_where('det_anggota', array('id_anggota' => $id, 'nama_berkas' => 'KTP'))->row();
    }
    public function getKK($id)
    {
        return $this->db->get_where('det_anggota', array('id_anggota' => $id, 'nama_berkas' => 'KK'))->row();
    }
    public function getBukti($id)
    {
        return $this->db->get_where('det_anggota', array('id_anggota' => $id, 'nama_berkas' => 'Bukti Transfer'))->row();
    }
    public function getFoto($id)
    {
        return $this->db->get_where('det_anggota', array('id_anggota' => $id, 'nama_berkas' => 'foto'))->row();
    }
    public function update()
    {
        $post = $this->input->post();
        $this->product_id = $post["id"];
        $this->name = $post["name"];
        $this->price = $post["price"];
        $this->description = $post["description"];
        return $this->db->update($this->_table, $this, array('product_id' => $post['id']));
    }

    public function delete($id)
    {
		$data = array(
        	'isdelete' => 0
		);
		$this->db->where('no_kta', $id);
		return $this->db->update('tb_anggota', $data);

    }
    public function detail($id)
    {
        return $this->db->query("SELECT tb_anggota.* ,tb_wilayah.wil as wil FROM tb_anggota JOIN tb_wilayah ON tb_anggota.wilayah = tb_wilayah.id WHERE no_kta = '$id'")->result();

    }	
    public function upload_berkas($id,$ktp,$file_element_name, $ket)
    {
        $data = array(
            'id_anggota'      => $id,
            'nama_berkas'      => $ktp,
            'berkas'      => $file_element_name,
            'ket'      => $ket
        );
        $this->db->insert('det_anggota', $data);
    }
    public function chart()
    {
        return $this->db->get('chart')->result();
    }
    public function g_lk()
    {
        return $this->db->get('gender1')->row();
    }
    public function g_pr()
    {
        return $this->db->get('gender2')->row();
    }
    public function count_kegiatan()
    {
        $month = date('m');
        return $this->db->query("SELECT count(id) as idx FROM tb_kegiatan WHERE jadwal LIKE '%2021-".$month."%'")->row(); // Produces an integer, like 17
    }

}
