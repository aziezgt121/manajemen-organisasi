-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 14, 2021 at 04:50 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_pao`
--

-- --------------------------------------------------------

--
-- Stand-in structure for view `data_anggota`
--
CREATE TABLE `data_anggota` (
`id` int(11)
,`no_kta` varchar(100)
,`nama_anggota` varchar(100)
,`wil` varchar(100)
,`foto` text
,`isdelete` enum('1','0')
);

-- --------------------------------------------------------

--
-- Table structure for table `det_anggota`
--

CREATE TABLE `det_anggota` (
  `id_anggota` varchar(100) DEFAULT NULL,
  `nama_berkas` text,
  `berkas` text,
  `ket` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `det_anggota`
--

INSERT INTO `det_anggota` (`id_anggota`, `nama_berkas`, `berkas`, `ket`) VALUES
('210205180531', 'KTP', 'KTP6020374fe153e5.PNG', 'Berkas Upload'),
('210205180531', 'KK', 'KK6020375e63d5e7.PNG', 'Berkas Upload'),
('210205180531', 'foto', 'FT6020376b511bdDes17,Doc1Page1.jpg', 'Berkas Upload'),
('210205180531', 'Bukti Transfer', 'BT6020377a448cb2.PNG', 'Berkas Upload'),
('210205175219', 'foto', 'FT6020a0dac3b67LOGOACP3-01(1).png', 'Berkas Upload'),
('210210031203', 'foto', 'FT602342ac58b87LogoFaeyzaKomputer_04.png', 'Berkas Upload'),
('210210045942', 'KTP', 'KTP60237b5e2cbff3.PNG', 'Berkas Upload'),
('210210045942', 'KK', 'KK60237b678aff64.PNG', 'Berkas Upload'),
('210210045942', 'foto', 'FT60237b75001535.PNG', 'Berkas Upload'),
('210210045942', 'Bukti Transfer', 'BT60237b7f1092e7.PNG', 'Berkas Upload');

-- --------------------------------------------------------

--
-- Table structure for table `riwayat_organisasi`
--

CREATE TABLE `riwayat_organisasi` (
  `id_anggota` varchar(100) DEFAULT NULL,
  `organisais` varchar(100) DEFAULT NULL,
  `jabatan` varchar(100) DEFAULT NULL,
  `priode` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `riwayat_organisasi`
--

INSERT INTO `riwayat_organisasi` (`id_anggota`, `organisais`, `jabatan`, `priode`) VALUES
('210210052223', 'ASGSDAGSDAG', 'Anggota', '2020'),
('210210052334', 'Partai Gerindra', 'Anggota', '2020');

-- --------------------------------------------------------

--
-- Table structure for table `tb_anggota`
--

CREATE TABLE `tb_anggota` (
  `id` int(11) NOT NULL,
  `no_kta` varchar(100) NOT NULL,
  `foto` text,
  `nama_anggota` varchar(100) DEFAULT NULL,
  `tempat_lahir` varchar(100) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `wilayah` int(11) DEFAULT NULL,
  `telp` char(12) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `nik` varchar(100) DEFAULT NULL,
  `alamat` text,
  `desa` text,
  `kec` text,
  `kab` text,
  `prov` text,
  `isdelete` enum('1','0') DEFAULT '1',
  `jk` varchar(100) DEFAULT NULL,
  `pos` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `tgl_gabung` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_anggota`
--

INSERT INTO `tb_anggota` (`id`, `no_kta`, `foto`, `nama_anggota`, `tempat_lahir`, `tgl_lahir`, `wilayah`, `telp`, `email`, `nik`, `alamat`, `desa`, `kec`, `kab`, `prov`, `isdelete`, `jk`, `pos`, `status`, `tgl_gabung`) VALUES
(15, '210203083720', '', 'Rudi', 'joioioij', '0008-08-07', 2, '8987897789', 'iouoiu', 'jijiojo', 'agdsgasdjio', '8oii', 'jijoi', 'oijoi', 'iji', '1', 'Perempuan', '897', '1', NULL),
(17, '210205175219', 'FT6020a0dac3b67LOGOACP3-01(1).png', 'Muhammad Ibrahim', 'Sungai hilir', '1991-12-31', 1, '082317413', 'admin@gmail.com', '1231312321', 'Idonesia', 'tagaraja', 'kateman', 'indragiri hilir', 'riau', '1', 'Laki - Laki', '29255', '0', NULL),
(18, '210205175641', 'acac3ab69a3150f5e0af1058bb2e9b49', 'jpojojpojp', 'ioho', '2006-12-31', 1, '3925372', 'admin@gmail.com', '0980', 'adsgsig', 'saf', 'jioj', 'iojioj', 'ijio', '', 'Laki - Laki', 'jioj', '1', NULL),
(19, '210205175823', '', 'jpojojpojp', 'ioho', '2006-12-31', 1, '3925372', 'admin@gmail.com', '0980', 'adsgsig', 'saf', 'jioj', 'iojioj', 'ijio', '', 'Laki - Laki', 'jioj', '1', NULL),
(20, '210205180205', 'acac3ab69a3150f5e0af1058bb2e9b49', 'jpojojpojp', 'ioho', '2006-12-31', 1, '3925372', 'admin@gmail.com', '0980', 'adsgsig', 'saf', 'jioj', 'iojioj', 'ijio', '', 'Laki - Laki', 'jioj', '1', NULL),
(21, '210205180531', 'acac3ab69a3150f5e0af1058bb2e9b49', 'jpojojpojp', 'ioho', '2006-12-31', 1, '3925372', 'admin@gmail.com', '0980', 'adsgsig', 'saf', 'jioj', 'iojioj', 'ijio', '', 'Laki - Laki', 'jioj', NULL, NULL),
(22, '210205180612', 'acac3ab69a3150f5e0af1058bb2e9b49', 'jpojojpojp', 'ioho', '2006-12-31', 1, '3925372', 'admin@gmail.com', '0980', 'adsgsig', 'saf', 'jioj', 'iojioj', 'ijio', '1', 'Laki - Laki', 'jioj', NULL, NULL),
(601, '210206214631', 'default.png', 'hasbullah', 'tembilahan', '2005-12-31', 1, '2332890324', 'admin@gmail.com', '1125155', 'sadioasgji', NULL, 'ojio', 'jioj', 'iojioj', '1', 'Laki - Laki', 'oijio', '0', NULL),
(60209, '210208030058', 'default.png', 'Rudi widodo', 'sdifjiao', '2021-12-31', 1, '23894', 'aioasf', '20340182', 'asdf', NULL, 'asdf', 'ad', 'asfd', '', 'Laki - Laki', 'asd', '0', NULL),
(60209, '210208030423', 'default.png\r\n', 'huihiuiuhui', '98iuhi', '0089-08-09', 0, '0732988479', 'aziezgt12@gmail.com', '8998987', 'jhgygiu', NULL, 'asdf', 'fg', 'sfg', '', 'Laki - Laki', '23423', '0', NULL),
(60235, '210210045942', 'FT60237b75001535.PNG', 'madit musyawwarah', 'sungai batang', '1996-12-31', 0, '910294902', 'admin@pppp.cm', '153510384', 'Indramayu selatan', 'Tagaraja12', 'Kateman', 'Indragiri Hilir', 'riau', '1', 'Laki - Laki', '23423', '0', '2021-02-10'),
(60235, '210210050228', 'default.png', 'Rudi Widodo', 'Sungai Bantang', '2000-12-31', 0, '0732988479', 'aziezgt@gmail.com', '1242114', '\r\nasagd', 'Tagaraja', 'asdf', 'Indragiri Hilir', 'Riau', '1', 'Laki - Laki', '28284', '0', '2021-02-10'),
(60235, '210210050609', 'default.png', 'Fathiyah', 'Sunga Guntung', '1953-12-31', 0, '0732988479', 'aziezgt@gmail.com', '329024', 'sadfdas', 'Tagaraja', 'Kateman', 'Indragiri Hilir', 'Riau', '1', 'Laki - Laki', '28284', '0', '2021-02-10'),
(60235, '210210051021', 'default.png', 'BAMBANG', 'oifjasjfo', '2021-12-31', 0, '082386340464', 'aziezgt@gmail.com', '0809809890', 'ASGD', 'Tagaraja', 'Kateman', 'Indragiri Hilir', 'Riau', '1', 'Laki - Laki', '28284', '0', '2021-02-10'),
(60235, '210210051043', 'default.png', 'BAMBANG', 'oifjasjfo', '2021-12-31', 0, '082386340464', 'aziezgt@gmail.com', '0809809890', 'ASGD', 'Tagaraja', 'Kateman', 'Indragiri Hilir', 'Riau', '1', 'Laki - Laki', '28284', '0', '2021-02-10'),
(60235, '210210051840', 'default.png', 'bakar abdu', 'Sunga Guntung', '1997-12-31', 0, '0732988479', 'aziezgt@gmail.com', '0809809890', 'gsdg', 'Tagaraja', 'Kateman', 'Indragiri Hilir', 'Riau', '1', 'Laki - Laki', '28284', '0', '2021-02-10'),
(60235, '210210052223', 'default.png', 'Programming', 'Sunga Guntung', '2021-12-30', 0, '0732988479', 'aziezgt@gmail.com', '0809809890', 'asd', 'Tagaraja', 'Kateman', 'Indragiri Hilir', 'Riau', '1', 'Perempuan', '28284', '0', '2021-02-10'),
(60235, '210210052334', 'default.png', 'balam sakti', 'Sunga Guntung', '1995-12-31', 0, '0732988479', 'aziezgt@gmail.com', '0809809890', 'wqwqeqw', 'Tagaraja', 'Kateman', 'Indragiri Hilir', 'Riau', '1', 'Laki - Laki', '28284', '0', '2021-02-10'),
(602340, '210210031203', 'FT602342ac58b87LogoFaeyzaKomputer_04.png', 'iojiojio', 'hohiohoih', '2002-12-31', 0, '908982189909', 'lakasds', '098908', 'skdanpok[', 'IOH', 'oijoij', 'oijoij', 'oijoij', '1', 'Laki - Laki', 'iojoij', '0', NULL),
(6023500, '210210051647', 'default.png', 'rahmat jaya', 'qwiouwruwq', '2021-12-31', 0, '082386340464', 'aziezgt@gmail.com', '239403402', 'fdsf', 'Tagaraja', 'Kateman', 'Indragiri Hilir', 'Riau', '1', 'Laki - Laki', '28284', '0', '2021-02-10'),
(601000000, '210206112801', 'default.png', 'Abdul Rahim', 'Sunga Guntung', '1995-12-31', 10, '12482149', 'admin@gmail.com', '1404082109960003', 'Indonesia Raya', NULL, 'Kateman', 'Indragiri Hilir', 'Riau', '1', 'Laki - Laki', '29255', '0', NULL),
(601000000, '210206112831', 'default.png', 'Abdul Rahim', 'Sunga Guntung', '1995-12-31', 123, '12482149', 'admin@gmail.com', '1404082109960003', 'Indonesia Raya', NULL, 'Kateman', 'Indragiri Hilir', 'Riau', '', 'Laki - Laki', '29255', '0', NULL),
(601000000, '210206112855', 'default.png', 'Abdul Rahim', 'Sunga Guntung', '1995-12-31', 1, '12482149', 'admin@gmail.com', '1404082109960003', 'Indonesia Raya', NULL, 'Kateman', 'Indragiri Hilir', 'Riau', '', 'Laki - Laki', '29255', '0', NULL),
(601000000, '210206112944', 'default.png', 'Abdul Rahim', 'Sunga Guntung', '1995-12-31', 1, '12482149', 'admin@gmail.com', '1404082109960003', 'Indonesia Raya', NULL, 'Kateman', 'Indragiri Hilir', 'Riau', '', 'Laki - Laki', '29255', '0', NULL),
(601000001, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL),
(601000002, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_kegiatan`
--

CREATE TABLE `tb_kegiatan` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `jadwal` datetime DEFAULT NULL,
  `desa` varchar(100) DEFAULT NULL,
  `kec` varchar(100) DEFAULT NULL,
  `kab` varchar(100) DEFAULT NULL,
  `alamat` text,
  `pj` varchar(100) DEFAULT NULL,
  `file` text,
  `catatan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kegiatan`
--

INSERT INTO `tb_kegiatan` (`id`, `nama`, `jadwal`, `desa`, `kec`, `kab`, `alamat`, `pj`, `file`, `catatan`) VALUES
(1596144003, 'oiioiojioji', '2019-12-31 11:59:00', 'oi', 'jioj', 'iojoi', 'jiojoij', 'iojoijio', 'file60223ae75fc7f1.PNG', 'Baiklah'),
(1659774131, 'namas', '2019-11-14 01:00:00', 'kec', 'prov', 'kab', 'alamat', 'pj', 'file6022571fa80fc2.PNG', 'catatan'),
(1838035688, 'Sunnatan massal', '2021-12-31 00:58:00', 'Tagaraja', 'asdf', 'Indragiri Hilir', 'Jalan Kartama', 'Muhmmad Ridwa', 'file60225743a775aSURATLAMARANMIRNA.pdf', 'ok');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `username` int(11) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `level` varchar(10) DEFAULT NULL,
  `anggota` int(11) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`username`, `password`, `level`, `anggota`, `nama`) VALUES
(0, '21232f297a57a5a743894a0e4a801fc3', '1', 1, 'Azis');

-- --------------------------------------------------------

--
-- Table structure for table `tb_wilayah`
--

CREATE TABLE `tb_wilayah` (
  `id` int(11) NOT NULL,
  `wil` varchar(100) DEFAULT NULL,
  `kec` varchar(100) DEFAULT NULL,
  `kab` varchar(100) DEFAULT NULL,
  `prov` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_wilayah`
--

INSERT INTO `tb_wilayah` (`id`, `wil`, `kec`, `kab`, `prov`) VALUES
(0, 'Guntung', 'kateman ', 'indragiri hilir ', 'riau'),
(10, 'tes', 'tes', 'twe', 'yeq'),
(123, 'Tembilahan', 'Tembilahan Hilir', 'Inhil ', 'Riau');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `phone_no` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure for view `data_anggota`
--
DROP TABLE IF EXISTS `data_anggota`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `data_anggota`  AS  select `tb_anggota`.`id` AS `id`,`tb_anggota`.`no_kta` AS `no_kta`,`tb_anggota`.`nama_anggota` AS `nama_anggota`,`tb_wilayah`.`wil` AS `wil`,`tb_anggota`.`foto` AS `foto`,`tb_anggota`.`isdelete` AS `isdelete` from (`tb_anggota` join `tb_wilayah` on((`tb_anggota`.`wilayah` = `tb_wilayah`.`id`))) where (`tb_anggota`.`isdelete` = 1) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_anggota`
--
ALTER TABLE `tb_anggota`
  ADD PRIMARY KEY (`id`,`no_kta`);

--
-- Indexes for table `tb_kegiatan`
--
ALTER TABLE `tb_kegiatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `tb_wilayah`
--
ALTER TABLE `tb_wilayah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_anggota`
--
ALTER TABLE `tb_anggota`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=601000003;
--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `username` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_wilayah`
--
ALTER TABLE `tb_wilayah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=124;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
